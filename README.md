# Ansible AWX
AWX is a web-based solution that makes Ansible more comfortable to use for IT teams. It is designed to be the hub for all automation tasks. It has a fantastic browser REST API and allows you to control access, graphically manage or sync inventory with a wide variety of cloud sources, log all your jobs, and integrate well with Lightweight Directory Access Protocol (LDAP).


# AWX Installation 
Here are some steps to deploy and run the Ansible playbook for the basic installation through the Ansible AWX dashboard, such as:


#System Requirements for AWX
o	4 GB of RAM
o	3.4 GHz CPU with 2 Cores
o	50 GB of hard disk space
o	Running Docker, 

Needs:

- #yum -y install epel-release
- #yum -y install ansible git python3-pip
- #yum -y install yum-utils device-mapper-persistent-data lvm2

Docker:

- #yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
- #yum -y install docker-ce --allowerasing
- #systemctl start docker


- pip3 install docker
- pip3 install docker-compose
- systemctl restart docker
- systemctl enable docker

Check Docker:


- #docker run hello-world

AWX:


- #git clone -b "9.1.1" https://github.com/ansible/awx.git
- cd awx/installer/
- Change into the inventory file:



- localhost ansible_connection=local ansible_python_interpreter="/usr/bin/env python3"
- postgres_data_dir=/var/lib/pgdocker
- awx_official=true
- project_data_dir=/var/lib/awx/projects
- awx_alternate_dns_servers="8.8.8.8,8.8.4.4"
- pg_admin_password=postgrespass@123
- admin_password=password
- Then run the AWX installer:





- #ansible-playbook -i inventory install.yml

If you want…

Check procceses:

- docker ps -a
- docker logs -f awx_task
- Login into AWX server:


- #docker container exec -it awx_task bash

Visit with Firefox and login with admin / password: http://awx_IP_here/



- Refernce : https://www.linuxtechi.com/install-ansible-awx-docker-compose-centos-8/



